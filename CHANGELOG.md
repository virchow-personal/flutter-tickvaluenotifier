## [0.0.2]

* Add tests
* Make flutter analyze happy

## [0.0.1] 

* Initial functioning code
