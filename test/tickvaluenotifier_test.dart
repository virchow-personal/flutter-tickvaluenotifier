import 'package:flutter_test/flutter_test.dart';

import 'package:tickvaluenotifier/tickvaluenotifier.dart';
import 'dart:async';

Future<int> getMeValue() {
  return Future.value(1);
}

void main() {
  test('How do I even test this?', () async {
    final v = TickValueNotifier<int>(
        0,
        getMeValue,
        Duration(milliseconds: 10),
        );
    await Future.delayed(Duration(milliseconds: 20), null);
    expect(v.value, 1);
  });
}
