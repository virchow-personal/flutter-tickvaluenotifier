# tickvaluenotifier

Flutter package to provide a ValueNotifier that makes a regular call at a timed
interval.

## Getting Started

[ValueNotifiers](https://api.flutter.dev/flutter/foundation/ValueNotifier-class.html)
are cool. You can use a special
[ValueListenableBuilder](https://api.flutter.dev/flutter/widgets/ValueListenableBuilder-class.html) builder for them that is called
when their value changes. I find them a very nice alternative to stream builders. So
much so I almost never use a stream builder.

Maybe an example makes it clearer...


```dart

/// I just return a string, but I could easily be async and call a server.
Future<string> fetch_name() {
  return Future.value("R. Daneel Olivaw");
}


class MyState {
  
  final name = TickValueNotifier<string>(
      "no name yet", fetch_name,  Duration(seconds: 10)));

}

class MyNameWidget extends StatelessWidget {
  final state = MyState();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      builder: (BuildContext context, string value, Widget child) {
        return Text(value);
      },
      valueListenable: state.name,
    );
  }
  
}

```
