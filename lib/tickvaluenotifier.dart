/// Library to regularly update a ValueNotifier from a callable at a regular
/// interval.
library tickvaluenotifier;

import 'dart:async';
import 'package:flutter/material.dart';

/// [ValueNotifier] that updates its internal value regularly by calling a
/// function.
class TickValueNotifier<T> extends ValueNotifier<T> {
  /// Creates a new [TickValueNotifier] with a default [value] which is updated
  /// at [timeout] regularity calling [callback].
  TickValueNotifier(T value, Future<T> callback(), Duration timeout)
      : super(value) {
    new Timer.periodic(
      timeout,
      (Timer t) => callback().then(
        (T v) {
          this.value = v;
        },
      ),
    );
  }
}
